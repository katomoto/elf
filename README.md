# elf

Embedded Language Framework - A simple scripting language designed for embedding safely and easily within a Java application. Very low memory overhead, very flat learning curve.